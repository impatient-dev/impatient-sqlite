# impatient-sqlite

Basic interfaces representing database connections.
The interfaces are very similar to the JDBC ones (except that indexes are 0-based in this library, not 1-based).
This library was originally written to abstract over the differences between JDBC and Android database connections,
though as of 2021-08-09, no Android code has been written to use this library.