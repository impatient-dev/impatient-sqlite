package imp.sqlite

/**A prepared statement: a compiled SQL statement that can be executed.
 * The SQL in a prepared statement may contain parameters, represented as question marks ("?").
 * Each param must have a value substituted for it (via setX) before the statement can be executed.
 * Indexes are 0-based.*/
interface PreparedStatement : AutoCloseable {
	fun setNull(i: Int)

	fun setBlob(i: Int, value: ByteArray)
	fun setBoolean(i: Int, value: Boolean)
	fun setDouble(i: Int, value: Double)
	fun setFloat(i: Int, value: Float)
	fun setInt(i: Int, value: Int)
	fun setLong(i: Int, value: Long)
	fun setShort(i: Int, value: Short)
	fun setString(i: Int, value: String)

	/**Sets a param of an unknown type. The value must be a type the database driver understands: null, primitives, String. */
	fun setParam(i: Int, value: Any?)
}


interface PreparedInsert : PreparedStatement {
	/**Executes the INSERT statement, and returns the generated primary key.*/
	fun insert(): Any
}

interface PreparedSelect : PreparedStatement {
	/**Executes the SELECT statement, and return a cursor for the results.*/
	fun select(): Cursor
}

interface PreparedUpdate : PreparedStatement {
	/**Executes the statement, and returns nothing.*/
	fun update()
}



inline fun <T> PreparedSelect.selectAll(rowParser: (Cursor) -> T): ArrayList<T> = select().use { cursor ->
	val out = ArrayList<T>()
	while(cursor.moveToNext())
		out.add(rowParser(cursor))
	return out
}