package imp.sqlite

/**An open connection to a SQLite database. ("Connection" was taken.)
 * Abstracts away the differences between the Android and JDBC versions.*/
interface Database : AutoCloseable {
	/**Begins a transaction.
	 * If this is not called, every statement will be in its own transaction.
	 * Nested transactions are supported: if you call this multiple times,
	 * the changes will not be committed until you call commit() the same number of times. */
	fun beginTransaction()
	fun commit()

	/**Rolls back the open transaction. If a nested transaction is in progress, all levels of nesting are rolled back.*/
	fun rollback()
	/**Rolls back any open transaction.*/
	fun rollbackOpt() {
		if(inTransaction)
			rollback()
	}

	/**Returns true if a transaction is open - false means we're in autocommit mode.*/
	val inTransaction: Boolean

	/**Prepares an INSERT statement that inserts a single record into a table.
	 * This type of prepared statement returns the primary key generated for the new record.*/
	fun prepareInsert(sql: String): PreparedInsert
	/**Prepares a SELECT statement. This type of prepared statement can generate a cursor to view the selected data.*/
	fun prepareSelect(sql: String): PreparedSelect
	/**Prepares a statement that modifies the database but does not return any information, such as an UPDATE or DELETE statement.*/
	fun prepareUpdate(sql: String): PreparedUpdate

	/**Sets whether foreign keys are enforced. Unless this is called, foreign keys are enforced by default.*/
	fun enforceForeignKeys(enforce: Boolean)
}


fun Database.exec(sql: String) = prepareUpdate(sql).use { it.update() }

/**Returns a list of rows matching the query, using the parser to turn rows into objects.*/
inline fun <T> Database.selectAll(sql: String, rowParser: (Cursor) -> T): ArrayList<T> {
	prepareSelect(sql).use { stmt ->
		stmt.select().use { cursor ->
			val out = ArrayList<T>()
			while (cursor.moveToNext())
				out.add(rowParser(cursor))
			return out
		}
	}
}

/**Runs the block for each record in the result.
 * Don't move the cursor (moveToNext etc.) - we'll do that for you.*/
inline fun Database.selectEach(sql: String, block: (Cursor) -> Unit) {
	prepareSelect(sql).use { stmt ->
		stmt.select().use { cursor ->
			while(cursor.moveToNext())
				block(cursor)
		}
	}
}

/**Returns the value of the 1 row that matches the query, if any. Throws an exception if multiple records match.*/
inline fun <T> Database.selectOnly(sql: String, rowParser: (Cursor) -> T): T? {
	prepareSelect(sql).use { stmt ->
		stmt.select().use { cursor ->
			if(!cursor.moveToNext())
				return null
			val out = rowParser(cursor)
			if(cursor.moveToNext())
				throw Exception("Multiple records matched the query.")
			return out
		}
	}
}

fun Database.selectOnlyBool(sql: String): Boolean? = selectOnly(sql) { it.getBoolean(0) }
fun Database.selectOnlyInt(sql: String): Int? = selectOnly(sql) { it.getInt(0) }
fun Database.selectOnlyLong(sql: String): Long? = selectOnly(sql) { it.getLong(0) }
fun Database.selectOnlyString(sql: String): String? = selectOnly(sql) { it.getString(0) }

/**Runs the block with foreign keys disabled, then re-enables them.*/
inline fun Database.withoutForeignKeys(block: () -> Unit) {
	enforceForeignKeys(false)
	try {
		block()
	} finally {
		enforceForeignKeys(true)
	}
}

/**Executes the block in a transaction. Commits if the block returns normally; rolls back if the block throws an exception (and then rethrows the exception).*/
inline fun <R> Database.withTransaction(block: () -> R): R {
	this.beginTransaction()
	var fail = false
	try {
		val out = block()
		return out
	} catch(e: Throwable) {
		fail = true
		this.rollback()
		throw e
	} finally {
		// commit cannot be in try{} or else returns in the block() will cause it to be skipped
		if(!fail)
			this.commit()
	}
}

/**Combination of use() and withTransaction().*/
inline fun Database.useWithTransaction(block: (Database) -> Unit) = this.use { this.withTransaction { block(this) } }