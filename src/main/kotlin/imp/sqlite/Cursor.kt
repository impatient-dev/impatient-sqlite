package imp.sqlite

/**Lets you iterate through the results of a query.
 * Starts before the 1st row - you have to call moveToNext() before getting anything.
 * All indices are 0-based.
 * All getters can return null. The "req" functions ("require") throw exceptions if the value is null.*/
interface Cursor : AutoCloseable {
	/**Moves to the next record and returns whether it exists.
	 * The cursor always starts before the first record. */
	fun moveToNext(): Boolean
	/**Throws an exception if there's no next row to move to.*/
	fun reqMoveToNext() {
		if(!moveToNext())
			throw IllegalStateException("No next row to move to.")
	}

	fun getBlob(column: Int): ByteArray?
	fun getBoolean(column: Int): Boolean?
	fun getDouble(column: Int): Double?
	fun getFloat(column: Int): Float?
	fun getInt(column: Int): Int?
	fun getLong(column: Int): Long?
	fun getShort(column: Int): Short?
	fun getString(column: Int): String?

	/**Returns the index of a column name.*/
	fun columnIndex(name: String): Int

	fun getBlob(column: String): ByteArray? = getBlob(columnIndex(column))
	fun getBoolean(column: String): Boolean? = getBoolean(columnIndex(column))
	fun getDouble(column: String): Double? = getDouble(columnIndex(column))
	fun getFloat(column: String): Float? = getFloat(columnIndex(column))
	fun getInt(column: String): Int? = getInt(columnIndex(column))
	fun getLong(column: String): Long? = getLong(columnIndex(column))
	fun getShort(column: String): Short? = getShort(columnIndex(column))
	fun getString(column: String): String? = getString(columnIndex(column))

	private inline fun <T> T?.nonNull(column: Int) = this ?: throw Exception("Column was null: $column")
	private inline fun <T> T?.nonNull(column: String) = this ?: throw Exception("Column was null: $column")

	fun reqBlob(column: Int): ByteArray = getBlob(column).nonNull(column)
	fun reqBlob(column: String): ByteArray = getBlob(column).nonNull(column)

	fun reqBoolean(column: Int): Boolean = getBoolean(column).nonNull(column)
	fun reqBoolean(column: String): Boolean = getBoolean(column).nonNull(column)

	fun reqDouble(column: Int): Double = getDouble(column).nonNull(column)
	fun reqDouble(column: String): Double = getDouble(column).nonNull(column)

	fun reqFloat(column: Int): Float = getFloat(column).nonNull(column)
	fun reqFloat(column: String): Float = getFloat(column).nonNull(column)

	fun reqInt(column: Int): Int = getInt(column).nonNull(column)
	fun reqInt(column: String): Int = getInt(column).nonNull(column)

	fun reqLong(column: Int): Long = getLong(column).nonNull(column)
	fun reqLong(column: String): Long = getLong(column).nonNull(column)

	fun reqString(column: Int): String = getString(column).nonNull(column)
	fun reqString(column: String): String = getString(column).nonNull(column)
}


/**Calls the block once for each record found. This function closes the cursor when done.*/
inline fun Cursor.forEach(block: (Cursor) -> Unit) = this.use {
	while(this.moveToNext())
		block(this)
}