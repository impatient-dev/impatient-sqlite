plugins {
	id("org.jetbrains.kotlin.jvm")
}

group = "imp-dev"
version = "0.0.0"

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":impatient-utils"))
	testImplementation(project(":impatient-junit4"))
}